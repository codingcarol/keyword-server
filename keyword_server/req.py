from aiohttp.web import (
    HTTPBadRequest,
    HTTPNotFound,
    Request,
    Response,
    StreamResponse,
    middleware
)
from typing import Callable, Awaitable, Optional
import aiohttp_jinja2

class BaseParam:
    def __init__(self, request: Request) -> None:
        h = request.headers
        self.remote: str = request.remote
        self.method: str = request.method
        self.relative_url: str = str(request.url.relative())

        self.proto: Optional[str] = h.get('X-Forwarded-Proto', None)
        self.real_ip: Optional[str] = h.get('X-Real-IP', None)
        self.forwarded_for: Optional[str] = h.get('X-Forwarded-For', None)

    @property
    def ip(self) -> str:
        if self.real_ip is not None:
            return self.real_ip
        else:
            # TODO: Use logging
            # print('X-Real-IP not found. Doing a test?')
            return self.remote


class SearchParam(BaseParam):
    def __init__(self, request: Request) -> None:
        super().__init__(request)

        self.ids = request.query.getall('id', [])
        self.mixed = int(request.query.get('mixed', 1))
        self.padding_location = int(request.query.get('location', 0))
        self.padding_size = int(request.query.get('padding', 0))


@middleware
async def charset_param(
        request: Request,
        handler: Callable[[Request], Awaitable[StreamResponse]]
        ) -> StreamResponse:

    """Middleware that changes the charset of a response based on
       the query parameter 'e'.  Accepted values of 'e' are:
         - 0/absent: UTF-8
         - 1: GB 18030
         - 2: Big5(-HKSCS, for better alignment with the WhatWG
              definition of "Big5").
       The parameter is validated, but ignored, for non-text content types.
    """
    try:
        charset = {
            '0': 'utf-8',
            '1': 'gb18030',
            '2': 'big5-hkscs',
        }[request.query.get('e', '0')]

    except KeyError as e:
        raise HTTPBadRequest(text="Invalid 'e' parameter") from e

    try:
        resp = await handler(request)
        if resp.content_type.startswith('text/'):
            if isinstance(resp, Response):
                # recalculate the response body with the new charset
                try:
                    text = resp.text
                    resp.charset = charset
                    resp.text = text
                except UnicodeEncodeError as e:
                    raise HTTPBadRequest(
                        text="Cannot represent response with this 'e'"
                    ) from e

            else:
                # this is a StreamResponse and we would have to shim its
                # write method; we can get away with not implementing this
                # because aiohttp_jinja2 doesn't use StreamResponses
                raise NotImplementedError
    except HTTPNotFound as e:
        context = {}
        return aiohttp_jinja2.render_template('good.html', request, context)
    return resp
