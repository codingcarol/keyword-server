import csv
import datetime
import fcntl
import os
import threading

from typing import (
    Any, IO, List, Optional, TextIO, Tuple, Union,
)
from aiohttp.web import Application

__all__ = ["DBContext", "KeywordDB"]


def timestamp() -> str:
    return datetime.datetime.utcnow().isoformat()


class FileLock:
    """Context manager that applies fcntl-based read and write locks to
       a file that's already open.  CAUTION: Closing the file will drop
       the lock regardless of whether this manager has been exited.
       This class must be kept in sync with the class of the same name
       in scripts/add-keywords."""
    def __init__(self, f: Union[IO[Any], int], *, write: bool = False):
        self.for_write = write
        if isinstance(f, int):
            self.fd = f
        else:
            self.fd = f.fileno()

    def __enter__(self) -> None:
        fcntl.lockf(self.fd,
                    fcntl.LOCK_EX if self.for_write else fcntl.LOCK_SH)

    def __exit__(self, *dontcare: Any) -> None:
        fcntl.lockf(self.fd, fcntl.LOCK_UN)


class DBContext:
    """Context manager for adding and removing database objects from the
       application context."""
    def __init__(self, app: Application):
        self.app = app

    def __enter__(self) -> None:
        self.app["keyword-db"] = KeywordDB(self.app["keywords"])
        self.app["keyword-db"].start_update_thread()

    def __exit__(self, *dontcare: Any) -> None:
        self.app["keyword-db"].stop_update_thread()
        del self.app["keyword-db"]


class KeywordDB:
    """A simple key-value database for sensitive keywords, based on a CSV
       file on disk.  Each keyword has an ID number, a script, and a
       last-access timestamp.
    """

    FIELDNAMES = ["created", "last_queried", "id", "script", "keyword"]

    def __init__(self, keyword_f: str) -> None:
        self.keyword_f = keyword_f
        with open(keyword_f, "rt", encoding="utf-8") as fp:
            with FileLock(fp):
                rd = csv.DictReader(fp)
                assert rd.fieldnames == self.FIELDNAMES
                keywords = {}
                last_queried = {}
                created = {}
                for row in rd:
                    id_ = int(row["id"])
                    keywords[id_] = (row["keyword"], row["script"])
                    last_queried[id_] = row["last_queried"]
                    created[id_] = row["created"]

        self.keywords = keywords
        self.created = created
        self.last_queried = last_queried
        self._last_queried_is_dirty = False
        self._update_thread: Optional[threading.Thread] = None
        self._update_lock = threading.Lock()
        self._update_startstop_lock = threading.Lock()
        self._update_stop_flag = threading.Event()

    def _do_update_on_disk(self, fp: TextIO) -> None:
        """Internal: actually update the CSV file on disk.
           Doesn't take any locks.  Should only ever be called by
           update_on_disk(), which does appropriate locking.
        """
        rd = csv.DictReader(fp)
        assert rd.fieldnames == self.FIELDNAMES
        updated = []
        new = []
        ids_updated = set()
        for row in rd:
            id_ = int(row["id"])
            if self.keywords.get(id_, (None,))[0] == row["keyword"]:
                row["last_queried"] = max(row["last_queried"],
                                          self.last_queried[id_])
                row["created"] = min(row["created"],
                                     self.created[id_])
                updated.append(row)
                ids_updated.add(id_)
            else:
                new.append(row)

        for id_ in self.keywords.keys():
            if id_ not in ids_updated:
                updated.append({
                    "id": str(id_),
                    "keyword": self.keywords[id_][0],
                    "script": self.keywords[id_][1],
                    "created": self.created[id_],
                    "last_queried": self.last_queried[id_]
                })

        updated.sort(key=lambda row: int(row["id"]))
        for i, row in enumerate(updated):
            if row["id"] != str(i+1):
                print(i+1, repr(row))
                assert False

        nextid = int(updated[-1]["id"]) + 1
        new.sort(key=lambda row: (int(row["id"]), row["keyword"]))
        for i, row in enumerate(new):
            row["id"] = str(nextid + i)

        updated.extend(new)

        del rd
        fp.seek(0)
        wr = csv.DictWriter(fp,
                            dialect="unix",
                            quoting=csv.QUOTE_MINIMAL,
                            fieldnames=self.FIELDNAMES)
        wr.writeheader()
        wr.writerows(updated)
        fp.flush()
        fp.truncate()
        os.fsync(fp.fileno())
        self._last_queried_is_dirty = False

    def update_on_disk(self) -> None:
        """Update the last_queried timestamps in keyword_f.
           This is done by rewriting the file in place; external
           accessors should take an fcntl(2) read or write lock on
           the file as appropriate.  ID numbers will remain stable
           over this operation unless an external update has
           introduced a conflict for a particular ID.
        """
        with self._update_lock:
            if not self._last_queried_is_dirty:
                return
            with open(self.keyword_f, "r+t", encoding="utf-8") as fp:
                with FileLock(fp, write=True):
                    self._do_update_on_disk(fp)

    def _update_thread_proc(self) -> None:
        """Internal: Thread procedure which calls update_on_disk() once every
           five minutes.  Should never be called directly.
        """
        while True:
            stop = self._update_stop_flag.wait(timeout=300)
            self.update_on_disk()
            if stop:
                break

    def start_update_thread(self) -> None:
        """Start a background thread that calls update_on_disk()
           periodically.  Does nothing if this thread is already running."""
        with self._update_startstop_lock:
            if self._update_thread is None:
                self._update_stop_flag.clear()
                self._update_thread = threading.Thread(
                    target=self._update_thread_proc,
                    name="KeywordDB-update-on-disk-" + str(id(self)))
                self._update_thread.start()

    def stop_update_thread(self) -> None:
        """Stop the background thread that calls update_on_disk()
           periodically.  Does nothing if the thread is not running."""
        with self._update_startstop_lock:
            if self._update_thread is not None:
                self._update_stop_flag.set()
                self._update_thread.join()
                self._update_thread = None

    def get(self, id_: int) -> Optional[Tuple[str, str]]:
        """Retrieve information for the keyword with ID number 'id_'."""
        return self.keywords.get(id_)

    def get_all_ids(self) -> List[int]:
        """Return a list of all the valid keyword numbers,
           in no particular order."""
        return list(self.keywords.keys())

    def touch(self, ids: List[int],
              ts: Optional[datetime.datetime] = None) -> None:
        """Update the timestamp for each of the keyword numbers IDS to
           TS, which defaults to the current time."""
        if ts is None:
            last_queried = timestamp()
        else:
            last_queried = ts.isoformat()

        dirty = False

        for id_ in ids:
            if self.last_queried[id_] < last_queried:
                self.last_queried[id_] = last_queried
                dirty = True

        self._last_queried_is_dirty |= dirty
