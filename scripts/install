#! /bin/sh

# Run this script as root, from the top of the git checkout, to
# install the keyword server as a system daemon.

if [ $# -gt 1 ]; then
    printf 'usage: ./scripts/install [destination]\n' >&2
    exit 1
fi
if [ "$(id -u)" -ne 0 ]; then
    printf '%s: error: must be run as root\n' "$0" >&2
    exit 1
fi
if id -u keyword-server > /dev/null 2>&1; then
    printf '%s: error: keyword-server user already exists\n' "$0" >&2
    exit 1
fi

PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin
export PATH
LC_ALL=C.UTF-8
export LC_ALL
LANGUAGE=C.UTF-8
export LANGUAGE
unset ENV BASH_ENV MAIL MAILPATH

INSTALLDIR="${1:-/home/keyword-server}"
NOLOGIN="$(which nologin)"
if [ -z "$NOLOGIN" ]; then
    NOLOGIN=/bin/false
fi

set -e

# Ensure Python 3.7 or later is installed,
# along with the necessary add-on modules.
is_at_least_python_37 () {
    if "$1" -c 'import sys; sys.exit(sys.version_info < (3,7,0))' \
            > /dev/null 2>&1
    then return 0
    else return 1
    fi
}

python=
# If the generic "python3" is sufficiently new, use it.
if is_at_least_python_37 python3; then
    python=python3
fi
if [ -z "$python" ]; then
    # Look for a sufficiently new "python3.X" already installed.
    # As of 2020-06-22 python 3.8 is the latest stable version.
    for minor in 7 8; do
        if is_at_least_python_37 python3.$minor; then
            python=python3.$minor
            break
        fi
    done
fi
# Look for a sufficiently new "python3.X" that we can install.
if [ -z "$python" ]; then
    for minor in 7 8; do
        if [ -n "$(apt-cache search '^python3\.'$minor'$')" ]; then
            python=python3.$minor
            break
        fi
    done
fi
if [ -z "$python" ]; then
    printf '%s\n' >&2 \
           "$0: could not find a packaged python that's new enough"
    exit 1
fi

# This will install the base interpreter as well as the -venv
# bolt-on, if necessary.
apt-get install ${python}-venv python3-systemd

# Make sure what we got is actually python 3.7 or later...
if is_at_least_python_37 $python
then :
else
    printf '%s\n' "$0: $python is not python 3.7 or later" >&2
    exit 1
fi

# ...and that the add-ons work.
ok=t
if "$python" -c 'import venv'
then :
else
    printf '%s\n%s\n' >&2 \
           "$0: $python: could not 'import venv'" \
           "Check whether the ${1}-venv package is installed and works."
    ok=
fi
if "$python" -c 'import systemd.daemon; import systemd.journal'
then :
else
     printf '%s\n%s\n%s\n' >&2 \
            "$0: $python: could not 'import systemd.{daemon,journal}." \
            "Check whether the python3-systemd package is installed" \
            "and works correctly with $1."
     ok=
fi
if [ -z "$ok" ]; then
    exit 1
fi

useradd -r -U -M -d "$INSTALLDIR" -s "$NOLOGIN" \
        -c "HTTP(S) keyword server" keyword-server
mkdir -p "$INSTALLDIR"
cp -r ./ "$INSTALLDIR/"

chmod -R a+rX,a-st,u+w,go-w "$INSTALLDIR"
chown -R keyword-server:keyword-server "$INSTALLDIR"

if [ -d /etc/systemd/system ] && systemctl get-default > /dev/null 2>&1; then
    sed -e "s:@INSTALLDIR@:${INSTALLDIR}:g" \
        < "$INSTALLDIR/scripts/keyword_server.service" \
        > /etc/systemd/system/keyword_server.service
    systemctl enable keyword_server.service
    printf '%s\n%s\n%s\n%s\n' \
           "The server has been installed into $INSTALLDIR but not started." \
           "Copy TLS keys into $INSTALLDIR, make sure they have appropriate" \
           "file permissions, then start the server with the command" \
           "# systemctl start keyword_server.service"
else
    printf '%s\n%s\n%s\n' \
           "The server has been installed into $INSTALLDIR." \
           "No arrangements have been made to start it at boot time." \
           "You will need to do that part manually."
fi

printf '%s\n' "Pre-populating the server's venv..."
su -s /bin/sh -l -c "$INSTALLDIR/scripts/update" keyword-server
