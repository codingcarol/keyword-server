from itertools import islice
from random import shuffle
from typing import Any, Awaitable, Callable, Dict, cast

from aiohttp.web import HTTPBadRequest, Request, StreamResponse
import aiohttp_jinja2

from . import req
from .lorem import lorem


# work around lack of proper type declaration for aiohttp_jinja2.template
InnerTemplateFnT = Callable[[Request], Any]
OuterTemplateFnT = Callable[[Request], Awaitable[StreamResponse]]
TemplateWrapperT = Callable[[InnerTemplateFnT], OuterTemplateFnT]


def template(*args: Any, **kwargs: Any) -> TemplateWrapperT:
    return cast(TemplateWrapperT, aiohttp_jinja2.template(*args, **kwargs))


# ASCII is a subset of all the encodings we are interested in so
# we need to rule out the possibility of a string being pure ASCII
# before we can say anything else about it.
def can_ascii(s: str) -> bool:
    try:
        s.encode("ascii")
        return True
    except UnicodeError:
        return False


# We use encodeability in Big5-HKSCS as a proxy for "this is
# traditional script".
def can_big5(s: str) -> bool:
    try:
        s.encode("big5-hkscs")
        return True
    except UnicodeError:
        return False


# Similarly, we use encodeability in GB 18030 *restricted to one- and
# two-byte sequences* as a proxy for "this is simplified script".
# (GB 18030's four-byte sequences cover all of Unicode, so that
# wouldn't tell us anything.)
def can_gb18030r(s: str) -> bool:
    try:
        e = s.encode("gb18030")
    except UnicodeError:
        return False

    i = 0
    limit = len(e)
    while i < limit:
        # Two-byte GB 18030 sequences have a first byte in the
        # 0x81 .. 0xFE range and a second byte in the 0x40 .. 0xFE
        # range.  Four-byte sequences also use the 0x81 .. 0xFE
        # range, with a second byte currently in the 0x30 .. 0x39
        # range; future extension space is available below 0x30 and
        # from 0x3a .. 0x3f.
        if e[i] > 0x80:
            i += 1
            if e[i] < 0x40:
                return False
        i += 1

    return True


@template('index.html')
async def index(request: Request) -> Dict[str, Any]:
    # query = db.keyword.insert()
    # values = [
    #     {"id": 2, "text": "example2",
    #      "last_queried": datetime.datetime.utcnow()},
    #     {"id": 3, "text": "example3",
    #      "last_queried": datetime.datetime.utcnow()},
    # ]
    #
    # keywords = await request.app['keyword-db'].get_all()
    return {'keywords': []}


@template('falundafa.html')
async def falundafa(request: Request) -> Dict[str, Any]:
    return {}


@template('whatisthis.html')
async def whatisthis(request: Request) -> Dict[str, Any]:
    '''
    Echo keyword with optional padding

    Parameters
    ----------
    k : str
        Word to echo
    s : str
        What script to use for padding text ('a', 's', 't', 'st')
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together.

    Example
    -------
    GET /search?k=ABC
    '''
    param = req.SearchParam(request)

    k = request.query.get('k', 'Huh')
    s = request.query.get('s', '-')
    if s == '-':
        if can_ascii(k):
            s = 'a'
        elif can_big5(k):
            s = 't'
        elif can_gb18030r(k):
            s = 's'
        else:
            s = 'st'
    else:
        if s not in ('a', 's', 't', 'st'):
            raise HTTPBadRequest(text="invalid 's' parameter")

    padding_size = param.padding_size
    padding_location = param.padding_location
    mixed = param.mixed

    padder = lorem(set(s))

    keywords = [
        {'id': -1, 'text': k, 'surrounding': padder.sentence()}
    ]

    if padding_location & 1:
        before = padder.paragraph(padding_size)
    else:
        before = ''
    if padding_location & 2:
        after = padder.paragraph(padding_size)
    else:
        after = ''

    return {
        'keywords': keywords,
        'before': before,
        'after': after,
        'mixed': mixed
    }


@template('whatisthis2.html')
async def whatisthis2(request: Request) -> Dict[str, Any]:
    k = request.query.get('k', 'Huh?')
    return {'k': k}


@template('good.html')
async def good(request: Request) -> Dict[str, Any]:
    return {}


@template('bad.html')
async def bad(request: Request) -> Dict[str, Any]:
    return {}


@template('sitemap.html')
async def sitemap(request: Request) -> Dict[str, Any]:
    '''
    Parameters
    ----------
    k : Optional[int]
        The number of keywords for a single URL. Default is 1.
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    n : int
        Maximum number of items returned. Default it's 1000. Maximum is 10000.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together (default).

    Example
    -------
    GET /sitemap?k=3&padding=1000&location=3&size=100
    '''
    k = int(request.query.get('k', 1))
    padding_size = int(request.query.get('padding', 0))
    padding_location = int(request.query.get('location', 0))
    n = min(10000, int(request.query.get('n', 1000)))
    mixed = int(request.query.get('mixed', 1))

    ids = request.app['keyword-db'].get_all_ids()
    shuffle(ids)

    qstrs = []
    for i in range(k, len(ids), k):
        qs = [f"id={ids[i - j]}" for j in range(1, k + 1)]
        qstrs.append('&'.join(qs))

    if len(ids) % k > 0:
        r = len(ids) % k
        qstrs.append('&'.join([f"id={ids[j]}"
                               for j in range(len(ids) - r, len(ids))]))

    urls = list(islice([
        f'/search?{qs}&padding={padding_size}'
        f'&location={padding_location}&mixed={mixed}'
        for qs in qstrs
    ], n))
    return {'urls': urls}


@template('search.html')
async def search(request: Request) -> Dict[str, Any]:
    '''
    Query keywords by ID(s) via a GET request.

    Parameters
    ----------
    id : [int]
        IDs of keywords.
    padding : int
        Size of padding in bytes. By default no padding is added.
    location : 0/1/2/3
        Location of padding in returned page.
        0: none; 1: in the front; 2: in the end; 3: both.
    mixed : 0/1
        Whether the keyword should be separated using HTML tags.
        0: separate; 1: mixed together.

    Example
    -------
    GET /search?id=1&id=2
    '''
    param = req.SearchParam(request)

    ids = param.ids
    padding_size = param.padding_size
    padding_location = param.padding_location
    mixed = param.mixed

    keywords = []
    scripts = set()
    touches = []

    for id_ in ids:
        idi = int(id_)
        ks = request.app['keyword-db'].get(idi)
        if ks is not None:
            keywords.append(ks[0])
            scripts.add(ks[1])
            touches.append(idi)

    padder = lorem(scripts)
    keywords = [
        {'text': k, 'surrounding': padder.sentence()}
        for k in keywords
    ]

    if padding_location & 1:
        before = padder.paragraph(padding_size)
    else:
        before = ''
    if padding_location & 2:
        after = padder.paragraph(padding_size)
    else:
        after = ''

    request.app['keyword-db'].touch(touches)

    return {
        'keywords': keywords,
        'before': before,
        'after': after,
        'mixed': mixed
    }
