#!/usr/bin/env python

"""A web server that generates pages containing sensitive keywords.
"""

import argparse
import asyncio
import collections
import logging
import re
import signal
import ssl
import sys
from typing import (
    Dict,
    NamedTuple,
    Optional,
    Pattern,
    Sequence,
)

import aiohttp
import aiohttp_jinja2
import jinja2
import toml

from . import daemon
from . import db
from . import route
from .req import charset_param


def make_ssl_context(cert: Optional[str] = None) -> ssl.SSLContext:
    ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ctx.set_alpn_protocols(['http/1.1'])
    if cert is not None:
        ctx.load_cert_chain(cert)
    return ctx


class CertRule(NamedTuple):
    pattern: Pattern[str]
    context: ssl.SSLContext


class HttpsConfig:
    def __init__(self, cfgfile: str, logger: logging.Logger):
        self.logger = logger
        self.cert_rules = []
        self.pre_sni_context = None
        # typing hack, this will always be replaced with one of the
        # collections.defaultdict objects below, but mypy needs an
        # explicit type declaration for it, in a single location
        self.port_rules: Dict[int, str] = {}

        if cfgfile is None:
            self.port_rules = collections.defaultdict(lambda: 'http')
        else:
            data = toml.load(cfgfile)
            try:
                default_protocol: str = data['ports'].pop('*', 'http')
                self.port_rules = collections.defaultdict(
                    lambda: default_protocol
                )
                any_https = (default_protocol == 'https')
                for port, protocol in data['ports'].items():
                    if protocol == 'https':
                        any_https = True
                    elif protocol != 'http':
                        raise ValueError("unsupported protocol: "+protocol)
                    self.port_rules[int(port)] = protocol

                cert_rules = data['certificates']
                if len(cert_rules) > 0:
                    if not any_https:
                        logger.warning(
                            "all ports unencrypted, certificate rules ignored"
                        )
                    else:
                        self.pre_sni_context = make_ssl_context()
                        self.pre_sni_context.sni_callback = self.sni_callback
                        cert_contexts = {}
                        for rule in cert_rules:
                            pattern = re.compile(
                                rule.get('hostpattern', '^.*$')
                            )
                            cert = rule['certificate']
                            if cert not in cert_contexts:
                                cert_contexts[cert] = make_ssl_context(cert)
                            self.cert_rules.append(CertRule(
                                pattern,
                                cert_contexts[cert]
                            ))

                elif any_https:
                    raise ValueError(
                        "HTTPS requested, but no server certificate rules"
                    )

            except Exception as e:
                raise ValueError(
                    f"config file {cfgfile} is invalid"
                ) from e

    def ssl_context_for(self,
                        s: daemon.ListeningSocket) -> Optional[ssl.SSLContext]:
        if self.port_rules[s.port] == 'https':
            return self.pre_sni_context
        else:
            return None

    def sni_callback(self,
                     sock: ssl.SSLObject,
                     host: str,
                     ctx: ssl.SSLContext) -> Optional[int]:

        for rule in self.cert_rules:
            if rule.pattern.match(host):
                sock.context = rule.context
                break
        else:
            raise RuntimeError("no certificate rule matches " + host)

        return None


async def run(app: aiohttp.web.Application,
              listen_addrs: Sequence[daemon.ListeningSocket],
              https_config: HttpsConfig,
              logger: logging.Logger) -> None:
    loop = asyncio.get_event_loop()
    stop_event = loop.create_future()

    def stop_signal() -> None:
        stop_event.set_result(None)

    loop.add_signal_handler(signal.SIGHUP, stop_signal)
    loop.add_signal_handler(signal.SIGINT, stop_signal)
    loop.add_signal_handler(signal.SIGINFO, stop_signal)
    loop.add_signal_handler(signal.SIGTERM, stop_signal)
    loop.add_signal_handler(signal.SIGXCPU, stop_signal)

    runner = aiohttp.web.AppRunner(app, handle_signals=False)
    await runner.setup()
    try:
        for lsock in listen_addrs:
            await aiohttp.web.SockSite(
                runner,
                lsock.socket,
                ssl_context=https_config.ssl_context_for(lsock)
            ).start()

        await loop.run_in_executor(None, daemon.notify_started)
        await stop_event

    finally:
        await runner.cleanup()


def main() -> None:
    loglevels = daemon.get_log_levels()

    ap = argparse.ArgumentParser(description=__doc__)
    ap.add_argument("-k", "--keywords",
                    type=str, default="keywords.csv",
                    help="CSV file containing keywords to use.")
    ap.add_argument("-l", "--log-file", type=str, default=None,
                    help="File to write logs to.  Defaults to stderr.")
    ap.add_argument("-L", "--log-level",
                    type=str.lower, choices=loglevels, default="info",
                    help="Level of detail for logging.  Defaults to 'info'.")

    ap.add_argument("https_config", type=str, nargs="?",
                    help="Configuration file indicating which protocol to"
                    " use for each listening port.  If not provided, will"
                    " serve unencrypted HTTP on all ports.")

    ap.add_argument("address", type=daemon.addr_and_port, nargs="*",
                    help="address:port to listen on, in addition to any"
                    " listening sockets supplied by systemd.")

    args = ap.parse_args()
    # If the https_config argument parses as an addr_and_port, it's not
    # a config file.
    if args.https_config is not None:
        try:
            args.address.append(daemon.addr_and_port(args.https_config))
            args.https_config = None

        except Exception:
            pass

    daemon.config_log(args.log_level, args.log_file)
    logger = logging.getLogger("keyword-server")

    try:
        https_config = HttpsConfig(args.https_config, logger)
        listening_sockets = daemon.preopened_listening_sockets()
        listening_sockets.extend(daemon.open_listening_sockets(args.address))
        if not listening_sockets:
            logger.error("No usable listening sockets")
            sys.exit(1)

        app = aiohttp.web.Application(
            middlewares=[charset_param],
            logger=logger
        )
        app["keywords"] = args.keywords

        aiohttp_jinja2.setup(
            app, loader=jinja2.PackageLoader("keyword_server", "templates"))
        route.setup(app)

        with db.DBContext(app):
            loop = asyncio.get_event_loop()
            try:
                loop.run_until_complete(run(app, listening_sockets,
                                            https_config, logger))
            finally:
                loop.run_until_complete(loop.shutdown_asyncgens())
                loop.close()

    except Exception:
        logger.critical(
            "fatal exception",
            exc_info=sys.exc_info())
        sys.exit(1)

    finally:
        logging.shutdown()


if __name__ == "__main__":
    main()
